import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy.spatial.distance import pdist, squareform

# 100 particles, with x,y,vx,vy
dots = np.random.rand(500,4)
# velocities on -0.5,0.5
dots[:,2:] -= 0.5
# scale velocities
dots[:,2:] *= 0.01

def main():
    fig, ax = plt.subplots(figsize=(15,15))

    ax.set_xlim(0,1)
    ax.set_ylim(0,1)
    
    xs = dots[:,0]
    ys = dots[:,1]
    
    line, = ax.plot([], [], 'b.', ms=30, )

    def animate(i):
        timestep()
        line.set_data(xs,ys)  # update the data
        #collision()
        return [line]
    
    ani = animation.FuncAnimation(fig, animate, 
                                  interval=25, 
                                  blit=True)

    # ani.save('basic_animation.mp4', fps=30, 
    #          extra_args=['-vcodec', 'libx264'])
    #or
    plt.show()
    
def collision():
    points = dots[:,:2]
    less_min = np.where(squareform(pdist(points)< 0.0002))

    dots[less_min, 2] *=0.0
    dots[less_min, 3] *=0.0

     # distance = pdist(points < 0.0000002)
   # dist = squareform(distance)

def timestep():
    dots[:,:2] += dots[:,2:]

    dots[:,3] -=0.001 # reduces the velocity e.g. gravity effect
    collision()
    touching_base = (dots[:,1] < 0.0) | (dots[:,1] > 1.0)
    touching_sides = (dots[:,0] < 0.0) | (dots[:,0] > 1.0)

    dots[ touching_base , 3  ] *= -0.9 # relatively reduced gain in velocity
    dots[touching_sides, 2] *=-1.0
    #dots[touching_top, 3] *=-1.0
    #dots[touching_sideR, 2] *=-1.0


main()













